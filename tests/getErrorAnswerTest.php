<?php

use MWClient\Entity\ErrorAnswer;

/**
 * Class GetErrorAnswerTest
 */
class getErrorAnswerTest extends Base
{
    private $service;

    
    public function setUp()
    {
        $this->service = static::getMockBuilder(\MWClient\MyWebinarClient::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();
    }


    /**
     * Test cases
     * @return array
     */
    public function testCases()
    {
        return [
            [
                '{"error": {"code":"404", "message":"Not found"}}',
                [
                    'result' => 'ok',
                    'data' => ['code' => 404, 'message' => 'Not found']
                ]
            ]
        ];
    }

    
    /**
     * @dataProvider testCases
     * @param $errorMessage
     * @param $expect
     */
    public function test_func($errorMessage, $expect)
    {
        switch ($expect['result']) {
            case 'ok':
                /** @var ErrorAnswer $result */
                $result = $this->invokeMethod($this->service, 'getErrorAnswer', [$errorMessage]);
                static::assertTrue($expect['data']['code'] === $result->getCode());
                static::assertTrue($expect['data']['message'] === $result->getMessage());
                break;

            default:
                static::expectException($expect['data']);
                $this->invokeMethod($this->service, 'getErrorAnswer', [$errorMessage]);
                break;
        }
    }
}
