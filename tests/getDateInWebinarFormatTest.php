<?php

/**
 * Class getDateInWebinarFormatTest
 */
class getDateInWebinarFormatTest extends PHPUnit_Framework_TestCase
{

    /**
     * @return array
     */
    public function testCases()
    {
        return [
            [
                '',
                [
                    'result' => 'exception',
                    'data' => Exception::class
                ],
            ],

            [
                '2016-10-01 18:21:44',
                [
                    'result' => 'ok',
                    'data' => [
                        'date' => [
                            'year' => 2016,
                            'month' => 10,
                            'day' => 01
                        ],
                        'time' => [
                            'hour' => 18,
                            'minute' => 21
                        ]
                    ]
                ],
            ],

            [
                '2016-10-01',
                [
                    'result' => 'ok',
                    'data' => [
                        'date' => [
                            'year' => 2016,
                            'month' => 10,
                            'day' => 01
                        ],
                        'time' => [
                            'hour' => 00,
                            'minute' => 00
                        ]
                    ]
                ],
            ],

            [
                '0',
                [
                    'result' => 'exception',
                    'data' => Exception::class
                ],
            ],

        ];
    }


    /**
     * @dataProvider testCases
     * @param $date
     * @param $expect
     */
    public function test_func($date, $expect)
    {
        switch ($expect['result']) {
            case 'ok':
                $result = \MWClient\Helpers\Utils::getDateInWebinarFormat($date);
                static::assertTrue($expect['data']['date']['year'] === $result['date']['year']);
                static::assertTrue($expect['data']['date']['month'] === $result['date']['month']);
                static::assertTrue($expect['data']['date']['day'] === $result['date']['day']);
                static::assertTrue($expect['data']['time']['hour'] === $result['time']['hour']);
                static::assertTrue($expect['data']['time']['minute'] === $result['time']['minute']);
                break;

            default:
                static::expectException($expect['data']);
                \MWClient\Helpers\Utils::getDateInWebinarFormat($date);
                break;
        }
    }
}
