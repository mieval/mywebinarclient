<?php
use MWClient\MyWebinarClient;

/**
 * Class initTest
 */
class initTest extends PHPUnit_Framework_TestCase
{

    /**
     * @return array
     */
    public function testCases()
    {
        return [
            // empty key
            [
                '',
                'https://this.url/for/api/client',
                [
                    'result' => 'exception',
                    'data' => Exception::class
                ],
                'empty key'
            ],

            // empty url
            [
                'sabbra-kadabra',
                '',
                [
                    'result' => 'exception',
                    'data' => Exception::class
                ],
                'empty url'
            ],

            // incorrect url
            [
                'sabbra-kadabra',
                'https',
                [
                    'result' => 'exception',
                    'data' => Exception::class
                ],
                'incorrect url'
            ],

            // normal case
            [
                'sabbra-kadabra',
                'https://this.url/for/api/client',
                [
                    'result' => 'ok',
                    'data' => [
                        'sabbra-kadabra',
                        'https://this.url/for/api/client'
                    ]
                ],
                'normal case'
            ],
        ];
    }


    /**
     * @dataProvider testCases
     * @param string $key
     * @param string $url
     * @param array $expect
     * @param string $message
     */
    public function test_initClient(string $key, string $url, array $expect, string $message)
    {
        switch ($expect['result']) {
            case 'ok':
                /** @var MyWebinarClient $instance */
                $client = new MyWebinarClient($key, $url);
                static::assertAttributeEquals($expect['data'][0], 'key', $client, $message);
                static::assertAttributeEquals($expect['data'][1], 'serviceUrl', $client, $message);
                break;

            default:
                static::expectException($expect['data']);
                new MyWebinarClient($key, $url);
                break;
        }
    }
}
