<?php

use Buzz\Browser;
use Buzz\Message\Response;
use MWClient\Entity\EventInfo;
use MWClient\Entity\EventSessionInfo;
use MWClient\MyWebinarClient;

/**
 * Class loadEventInfoTest
 */
class loadEventInfoTest extends PHPUnit_Framework_TestCase
{

    /** @var MyWebinarClient */
    protected $service;

    /** @var Browser */
    protected $browser;

    /** @var Response */
    protected $response;


    /**
     * Before every test
     */
    public function setUp()
    {
        $this->service = static::getMockBuilder(MyWebinarClient::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();

        $this->browser = static::getMockBuilder(Browser::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();

        $this->response = static::getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->setMethods(['isSuccessful', 'getContent'])
            ->getMock();

        $this->browser->method('get')->will(static::returnValue($this->response));
        $this->service->setBrowser($this->browser);
    }


    /**
     * @return array
     */
    public function testCases()
    {
        return [
            [
                173637,
                true,
                '{"id":173637,"name":"test","description":"test","status":"STOP","access":4,"lang":"RU","startsAt":"2016-11-14T13:45:00+0300","utcStartsAt":1479120300,"createUserId":1039267,"timezoneId":1,"endsAt":"2017-11-14T23:59:00+0300","duration":"PT1H0M0S","organizationId":52807,"type":"webinar","rule":"FREQ=DAILY;COUNT=1","lectors":[],"tags":[],"announceFiles":[],"files":[],"eventSessions":[{"id":182619,"name":"test","description":"test","status":"STOP","access":4,"lang":"RU","startsAt":"2016-11-14T13:46:38+0300","utcStartsAt":1479120398,"createUserId":1039267,"timezoneId":1,"endsAt":"2016-11-14T13:48:30+0300","duration":"PT1H0M0S","organizationId":52807,"type":"webinar","lectors":[],"tags":[],"announceFiles":[],"files":[{"id":5242525,"type":"file","fileType":"slide","createUserId":1039267,"organizationId":52807,"name":"\u041f\u0443\u0441\u0442\u043e\u0439 \u0441\u043b\u0430\u0439\u0434","url":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAIAAAC0SDtlAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wgUDwgXgOHHoQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAF0lEQVQY02P8\/\/8\/AymAiYFEMKqBJhoAMWADD70N1KIAAAAASUVORK5CYII="},{"id":5242501,"parentId":3700081,"type":"file","fileType":"record","createUserId":1039267,"name":"test","url":"https:\/\/events-storage.webinar.ru\/api-storage\/files\/webinar\/2016\/11\/14\/OZi27K4POXNTNC1oYikzumTrlCmbxz3V0VKFlZeSO1SOFti8.png","duration":111.77230000496,"description":"test","cuts":[],"isViewable":false}]}]}',
                [
                    'result' => 'ok',
                    'data' => [
                        (new EventInfo)
                            ->setId(173637)
                            ->setName('test')
                            ->setDescription('test')
                            ->setStatus('STOP')
                            ->setAccess(4)
                            ->setLang('RU')
                            ->setStartsAt('2016-11-14T13:45:00+0300')
                            ->setUtcStartsAt(1479120300)
                            ->setCreateUserId(1039267)
                            ->setTimezoneId(1)
                            ->setEndsAt('2017-11-14T23:59:00+0300')
                            ->setDuration('PT1H0M0S')
                            ->setOrganizationId(52807)
                            ->setType('webinar')
                            ->setRule('FREQ=DAILY;COUNT=1')
                            ->setEventSessions([
                                (new EventSessionInfo)
                                    ->setId(182619)
                                    ->setName('test')
                                    ->setDescription('test')
                                    ->setStatus('STOP')
                                    ->setAccess(4)
                                    ->setLang('RU')
                                    ->setStartsAt('2016-11-14T13:46:38+0300')
                                    ->setUtcStartsAt(1479120398)
                                    ->setCreateUserId(1039267)
                                    ->setTimezoneId(1)
                                    ->setEndsAt('2016-11-14T13:48:30+0300')
                                    ->setDuration('PT1H0M0S')
                                    ->setOrganizationId(52807)
                                    ->setType('webinar')
                                    ->setFiles([
                                        [
                                            'id' => 5242525,
                                            'type' => 'file',
                                            'fileType' => 'slide',
                                            'createUserId' => 1039267,
                                            'organizationId' => 52807,
                                            'name' => 'Пустой слайд',
                                            'url' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAIAAAC0SDtlAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wgUDwgXgOHHoQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAF0lEQVQY02P8//8/AymAiYFEMKqBJhoAMWADD70N1KIAAAAASUVORK5CYII=',                                            
                                        ],
                                        [
                                            'id' => 5242501,
                                            'parentId' => 3700081,
                                            'type' => 'file',
                                            'fileType' => 'record',
                                            'createUserId' => 1039267,
                                            'name' => 'test',
                                            'url' => 'https://events-storage.webinar.ru/api-storage/files/webinar/2016/11/14/OZi27K4POXNTNC1oYikzumTrlCmbxz3V0VKFlZeSO1SOFti8.png',
                                            'duration' => 111.77230000496,
                                            'description' => 'test',
                                            'cuts' => [],
                                            'isViewable' => false
                                        ]
                                    ])
                            ])
                            
                    ]
                ],


            ]
        ];
    }


    /**
     * @dataProvider testCases
     * @param $eventId
     * @param $isSuccessful
     * @param $getContent
     * @param $expect
     */
    public function test_func($eventId, $isSuccessful, $getContent, $expect)
    {
        $this->response->method('getContent')->will(static::returnValue($getContent));
        $this->response->method('isSuccessful')->will(static::returnValue($isSuccessful));

        switch ($expect['result']) {
            case 'ok':
                $result = $this->service->loadEventInfo($eventId);
                static::assertEquals($expect['data'][0], $result);
                break;

            default:
                static::expectException($expect['data']);
                $this->service->loadEventInfo($eventId);
                break;
        }
    }
}
