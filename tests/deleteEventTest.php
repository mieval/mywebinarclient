<?php
use Buzz\Browser;
use Buzz\Message\Response;
use MWClient\MyWebinarClient;

/**
 * Class deleteEventTest
 */
class deleteEventTest extends Base
{
    /** @var MyWebinarClient */
    protected $service;

    /** @var Browser */
    protected $browser;

    /** @var Response */
    protected $response;


    /**
     * Before every test
     */
    public function setUp()
    {
        $this->service = static::getMockBuilder(MyWebinarClient::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();

        $this->browser = static::getMockBuilder(Browser::class)
            ->disableOriginalConstructor()
            ->setMethods(['delete'])
            ->getMock();

        $this->response = static::getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->setMethods(['isSuccessful', 'getContent'])
            ->getMock();

        $this->browser->method('delete')->will(static::returnValue($this->response));
        $this->service->setBrowser($this->browser);
    }


    /**
     * Test cases
     */
    public function testCases()
    {
        return [
            [
                0,
                null,
                null,
                [
                    'result' => 'exception',
                    'data' => Exception::class
                ],
            ],

            [
                1,
                true,
                null,
                [
                    'result' => 'ok',
                    'data' => true
                ],
            ],

            [
                1,
                false,
                '{"error" : {"code":"500", "message":"Opps"}}',
                [
                    'result' => 'exception',
                    'data' => Exception::class
                ],
            ],
        ];
    }

    /**
     * @dataProvider testCases
     */
    public function test_func($eventId, $isSuccessful, $getContent, $expect)
    {
        $this->response->method('isSuccessful')->will(static::returnValue($isSuccessful));

        switch ($expect['result']) {
            case 'ok':
                $result = $this->service->deleteEvent($eventId);
                static::assertTrue(null === $result);
                break;

            default:
                $this->response->method('getContent')->will(static::returnValue($getContent));
                static::expectException($expect['data']);
                $this->service->deleteEvent($eventId);
                break;
        }
    }
}
