<?php
use Buzz\Browser;
use Buzz\Message\Response;
use MWClient\Entity\Record;
use MWClient\Entity\RecordsRequest;
use MWClient\MyWebinarClient;

/**
 * Class loadRecordsTest
 */
class loadRecordsTest extends Base
{
    /** @var MyWebinarClient */
    protected $service;

    /** @var Browser */
    protected $browser;

    /** @var Response */
    protected $response;


    /**
     * Before every test
     */
    public function setUp()
    {
        $this->service = static::getMockBuilder(MyWebinarClient::class)
            ->disableOriginalConstructor()
            ->setMethods()
            ->getMock();

        $this->browser = static::getMockBuilder(Browser::class)
            ->disableOriginalConstructor()
            ->setMethods(['get'])
            ->getMock();

        $this->response = static::getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->setMethods(['isSuccessful', 'getContent'])
            ->getMock();

        $this->browser->method('get')->will(static::returnValue($this->response));
        $this->service->setBrowser($this->browser);
    }


    /**
     * Test cases
     */
    public function testCases()
    {
        return [
            [
                null,
                false,
                '{"error": {"code":"404", "message":"Not found"}}',
                [
                    'result' => 'exception',
                    'data' => Exception::class
                ],
            ],

            [
                null,
                true,
                json_encode([
                    (object)[
                        "id" => 812117,
                        "name" => "Новое мероприятие сегодня",
                        "link" => "http://events.webinar.ru/1929/4290/record/5043",
                        "isViewable" => true,
                        "hasPassword" => false,
                        "size" => 0,
                        "createAt" => "2016-01-20 13:11:33"
                    ],
                ]),
                [
                    'result' => 'ok',
                    'data' => [
                        (new Record)
                            ->setId(812117)
                            ->setName("Новое мероприятие сегодня")
                            ->setLink("http://events.webinar.ru/1929/4290/record/5043")
                            ->setIsViewable(true)
                            ->setHasPassword(false)
                            ->setSize(0)
                            ->setCreateAt("2016-01-20 13:11:33")
                    ]
                ],
            ],

            [
                (new RecordsRequest)
                    ->setId(2)
                ,
                true,
                json_encode([
                    (object)[
                        "id" => 2,
                        "name" => "Новое мероприятие сегодня",
                        "link" => "http://events.webinar.ru/1929/4290/record/5043",
                        "isViewable" => true,
                        "hasPassword" => false,
                        "size" => 0,
                        "createAt" => "2016-01-20 13:11:33"
                    ],
                ]),
                [
                    'result' => 'ok',
                    'data' => [
                        (new Record)
                            ->setId(2)
                            ->setName("Новое мероприятие сегодня")
                            ->setLink("http://events.webinar.ru/1929/4290/record/5043")
                            ->setIsViewable(true)
                            ->setHasPassword(false)
                            ->setSize(0)
                            ->setCreateAt("2016-01-20 13:11:33")
                    ]
                ],
            ],
        ];
    }

    /**
     * @dataProvider testCases
     * @param $recordRequest
     * @param $isSuccessful
     * @param $getContent
     * @param $expect
     */
    public function test_func($recordRequest, $isSuccessful, $getContent, $expect)
    {
        $this->response->method('getContent')->will(static::returnValue($getContent));
        $this->response->method('isSuccessful')->will(static::returnValue($isSuccessful));

        switch ($expect['result']) {
            case 'ok':
                /** @var Record[] $recordsList */
                $recordsList = $this->service->loadRecords($recordRequest);
                /** @var Record $records */
                $record = $recordsList[0];

                /** @var Record $expectData */
                $expectData = $expect['data'][0];

                static::assertEquals($expect['data'], $recordsList);
                static::assertTrue($expectData->getId() === $record->getId());
                static::assertTrue($expectData->getCreateAt() === $record->getCreateAt());
                static::assertTrue($expectData->getHasPassword() === $record->getHasPassword());
                static::assertTrue($expectData->getIsViewable() === $record->getIsViewable());
                static::assertTrue($expectData->getLink() === $record->getLink());
                static::assertTrue($expectData->getName() === $record->getName());
                static::assertTrue($expectData->getSize() === $record->getSize());
                break;

            default:
                static::expectException($expect['data']);
                $this->service->loadRecords($recordRequest);
                break;
        }
    }
}
