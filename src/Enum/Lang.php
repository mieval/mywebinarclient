<?php

namespace MWClient\Enum;

/**
 * Class Lang
 * @package MWClient\Enum
 */
class Lang
{
    const RU = 'RU';
    const EN = 'EN';
    const UK = 'UK';
    const TR = 'TR';
    

    public static $enum = [
        self::RU,
        self::EN,
        self::UK,
        self::TR,
    ];
}