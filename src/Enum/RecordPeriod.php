<?php

namespace MWClient\Enum;

/**
 * Class RecordPeriod
 * @package MWClient\Enum
 */
class RecordPeriod
{
    const DAY = 'day';
    const WEEK = 'week';
    const MONTH = 'month';
    const YEAR = 'year';


    public static $enum = [
        self::DAY,
        self::WEEK,
        self::MONTH,
        self::YEAR,
    ];
}