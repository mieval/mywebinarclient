<?php

namespace MWClient\Enum;

/**
 * Class Access
 * @package MWClient\Enum
 */
class Access
{
    const OPEN = 1;
    const PASSWORD = 3;
    const WITHOUT_MODERATION_WITHOUT_PASSWORD = 4;
    const WITHOUT_MODERATION_WITH_PASSWORD = 6;
    const WITH_MODERATION_WITHOUT_PASSWORD = 8;
    const WITH_MODERATION_WITH_PASSWORD = 10;


    public static $enum = [
        self::OPEN,
        self::PASSWORD,
        self::WITHOUT_MODERATION_WITHOUT_PASSWORD,
        self::WITHOUT_MODERATION_WITH_PASSWORD,
        self::WITH_MODERATION_WITHOUT_PASSWORD,
        self::WITH_MODERATION_WITH_PASSWORD,
    ];
}