<?php

namespace MWClient\Enum;

/**
 * Class EventStatus
 * @package MWClient\Enum
 */
class EventStatus
{
    const ACTIVE = 'ACTIVE';
    const STOP = 'STOP';
    const RESERVED = 'RESERVED';


    public static $enum = [
        self::ACTIVE,
        self::STOP,
        self::RESERVED,
    ];
}