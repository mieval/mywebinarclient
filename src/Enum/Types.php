<?php

namespace MWClient\Enum;

/**
 * Class Types
 * @package MWClient\Enum
 */
class Types
{
    const TRUE = 'true';
    const FALSE = 'false';


    public static $boolEnum = [
        self::TRUE,
        self::FALSE,
    ];
}