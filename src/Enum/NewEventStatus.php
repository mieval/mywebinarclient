<?php

namespace MWClient\Enum;


class NewEventStatus
{
    const ACTIVE  = 'ACTIVE';
    const STOP  = 'STOP';

    public static $enum = [
        self::ACTIVE,
        self::STOP,
    ];
}