<?php

namespace MWClient\Enum;

/**
 * Class Roles
 * @package Olymp\Bundle\WebinarBundle\Api\Entity
 */
class Roles
{
    const GUEST = 'GUEST';
    const LECTURER = 'LECTURER';
    const ADMIN = 'ADMIN';

    public static $roleEnum = [
        self::GUEST,
        self::LECTURER,
        self::ADMIN
    ];
}