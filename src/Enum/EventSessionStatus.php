<?php

namespace MWClient\Enum;

/**
 * Class EventSessionStatus
 * @package MWClient\Enum
 */
class EventSessionStatus
{
    const ACTIVE = 'ACTIVE';
    const START = 'START';
    const STOP = 'STOP';


    public static $enum = [
        self::ACTIVE,
        self::START,
        self::STOP,
    ];
}