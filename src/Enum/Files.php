<?php

namespace MWClient\Enum;

/**
 * Class Files
 * @package MWClient\Enum
 */
class Files
{
    const VIDEO = 'video';
    const PRESENTATION = 'presentation';
    const SLIDE = 'slide';
    const TEST = 'test';
    const TEST_RESULT = 'testResult';
    const RECORD = 'record';
    const CONVERTED_RECORD = 'convertedRecord';


    public static $enum = [
        self::VIDEO,
        self::PRESENTATION,
        self::SLIDE,
        self::TEST,
        self::TEST_RESULT,
        self::RECORD,
        self::CONVERTED_RECORD,
    ];
}
