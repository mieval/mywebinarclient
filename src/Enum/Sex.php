<?php

namespace MWClient\Enum;


class Sex
{
    const MALE = 'male';
    const FEMALE = 'female';

    public static $sexEnum = [
        self::MALE,
        self::FEMALE
    ];
}