<?php

namespace MWClient\Enum;

/**
 * Class interval
 * @package MWClient\Enum
 */
class Interval
{
    const INTERVAL_15_MINUTES = 'PT0H15M0S';
    const INTERVAL_30_MINUTES = 'PT0H30M0S';
    const INTERVAL_45_MINUTES = 'PT0H45M0S';
    const INTERVAL_1_HOUR = 'PT1H0M0S';
    const INTERVAL_1_HOUR_30_MINUTES = 'PT1H30M0S';
    const INTERVAL_2_HOUR = 'PT2H00M0S';


    public static $enum = [
        self::INTERVAL_15_MINUTES,
        self::INTERVAL_30_MINUTES,
        self::INTERVAL_45_MINUTES,
        self::INTERVAL_1_HOUR,
        self::INTERVAL_1_HOUR_30_MINUTES,
        self::INTERVAL_2_HOUR,
    ];
}