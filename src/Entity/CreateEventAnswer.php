<?php

namespace MWClient\Entity;

/**
 * Class CreateEventAnswer
 * @package MWClient\Entity
 */
class CreateEventAnswer
{
    /** @var string */
    private $eventId;
    
    /** @var string */
    private $link;

    /**
     * @return string
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param string $eventId
     * @return CreateEventAnswer
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return CreateEventAnswer
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }
    
    
}