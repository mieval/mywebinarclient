<?php

namespace MWClient\Entity;

/**
 * Class Event
 * @package MWClient\Entity
 */
class Event extends BaseEvent
{
    /** @var string */
    private $status;

    /** @var string */
    private $urlAlias;

    /** @var string */
    private $rule;

    /** @var array */
    private $exceptionDates;

    /** @var string */
    private $isEventRegAllowed;

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Event
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlAlias()
    {
        return $this->urlAlias;
    }

    /**
     * @param string $urlAlias
     * @return Event
     */
    public function setUrlAlias($urlAlias)
    {
        $this->urlAlias = $urlAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @param string $rule
     * @return Event
     */
    public function setRule($rule)
    {
        $this->rule = $rule;
        return $this;
    }

    /**
     * @return array
     */
    public function getExceptionDates()
    {
        return $this->exceptionDates;
    }

    /**
     * @param array $exceptionDates
     * @return Event
     */
    public function setExceptionDates($exceptionDates)
    {
        $this->exceptionDates = $exceptionDates;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsEventRegAllowed()
    {
        return $this->isEventRegAllowed;
    }

    /**
     * @param string $isEventRegAllowed
     * @return Event
     */
    public function setIsEventRegAllowed($isEventRegAllowed)
    {
        $this->isEventRegAllowed = $isEventRegAllowed;
        return $this;
    }
}
