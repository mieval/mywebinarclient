<?php

namespace MWClient\Entity;

/**
 * Class EventInfo
 * @package MWClient\Entity
 */
class EventInfo
{
    /** @var int */
    private $id; 

    /** @var string */
    private $name; 

    /** @var string */
    private $description; 

    /** @var string */
    private $status; 

    /** @var int */
    private $access; 

    /** @var string */
    private $lang; 

    /** @var string */
    private $startsAt; 

    /** @var string */
    private $utcStartsAt; 

    /** @var int */
    private $createUserId; 

    /** @var int */
    private $timezoneId; 

    /** @var string */
    private $endsAt; 

    /** @var string */
    private $duration; 

    /** @var int */
    private $organizationId; 

    /** @var string */
    private $type; 

    /** @var string */
    private $rule; 

    /** @var array */
    private $lectors = [];

    /** @var array */
    private $tags = [];

    /** @var array */
    private $announceFiles = [];

    /** @var array */
    private $files = [];

    /** @var EventSessionInfo[] */
    private $eventSessions = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return EventInfo
     */
    public function setId(int $id): EventInfo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return EventInfo
     */
    public function setName(string $name): EventInfo
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return EventInfo
     */
    public function setDescription(string $description): EventInfo
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return EventInfo
     */
    public function setStatus(string $status): EventInfo
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @param int $access
     * @return EventInfo
     */
    public function setAccess(int $access): EventInfo
    {
        $this->access = $access;
        return $this;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return EventInfo
     */
    public function setLang(string $lang): EventInfo
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @param string $startsAt
     * @return EventInfo
     */
    public function setStartsAt(string $startsAt): EventInfo
    {
        $this->startsAt = $startsAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtcStartsAt()
    {
        return $this->utcStartsAt;
    }

    /**
     * @param string $utcStartsAt
     * @return EventInfo
     */
    public function setUtcStartsAt(string $utcStartsAt): EventInfo
    {
        $this->utcStartsAt = $utcStartsAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * @param int $createUserId
     * @return EventInfo
     */
    public function setCreateUserId(int $createUserId): EventInfo
    {
        $this->createUserId = $createUserId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTimezoneId()
    {
        return $this->timezoneId;
    }

    /**
     * @param int $timezoneId
     * @return EventInfo
     */
    public function setTimezoneId(int $timezoneId): EventInfo
    {
        $this->timezoneId = $timezoneId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }

    /**
     * @param string $endsAt
     * @return EventInfo
     */
    public function setEndsAt(string $endsAt): EventInfo
    {
        $this->endsAt = $endsAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     * @return EventInfo
     */
    public function setDuration(string $duration): EventInfo
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param int $organizationId
     * @return EventInfo
     */
    public function setOrganizationId(int $organizationId): EventInfo
    {
        $this->organizationId = $organizationId;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return EventInfo
     */
    public function setType(string $type): EventInfo
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @param string $rule
     * @return EventInfo
     */
    public function setRule(string $rule): EventInfo
    {
        $this->rule = $rule;
        return $this;
    }

    /**
     * @return array
     */
    public function getLectors()
    {
        return $this->lectors;
    }

    /**
     * @param array $lectors
     * @return EventInfo
     */
    public function setLectors(array $lectors): EventInfo
    {
        $this->lectors = $lectors;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return EventInfo
     */
    public function setTags(array $tags): EventInfo
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnnounceFiles()
    {
        return $this->announceFiles;
    }

    /**
     * @param array $announceFiles
     * @return EventInfo
     */
    public function setAnnounceFiles(array $announceFiles): EventInfo
    {
        $this->announceFiles = $announceFiles;
        return $this;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param array $files
     * @return EventInfo
     */
    public function setFiles(array $files): EventInfo
    {
        $this->files = $files;
        return $this;
    }

    /**
     * @return EventSessionInfo[]
     */
    public function getEventSessions()
    {
        return $this->eventSessions;
    }

    /**
     * @param EventSessionInfo[] $eventSessions
     * @return EventInfo
     */
    public function setEventSessions(array $eventSessions): EventInfo
    {
        $this->eventSessions = $eventSessions;
        return $this;
    }
}
