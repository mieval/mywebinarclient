<?php

namespace MWClient\Entity;

/**
 * Class EventRequest
 * @package MWClient\Entity
 */
class EventRequest
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $access;

    /** @var string */
    protected $status;

    /** @var string */
    protected $password;

    /** @var string */
    protected $description;

    /** @var string */
    protected $lang;

    /** @var string */
    protected $timezone;

    /** @var string */
    protected $urlAlias;

    /** @var array */
    protected $startsAt;

    /** @var array */
    protected $endsAt;

    /** @var string */
    protected $rule;

    /** @var string */
    protected $duration;

    /** @var string */
    protected $isAchive;

    /** @var array */
    protected $additionalFields;

    /** @var string */
    protected $image;

    /** @var array */
    protected $lectorIds;

    /** @var array */
    protected $tags;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @param string $access
     * @return self
     */
    public function setAccess($access)
    {
        $this->access = $access;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return self
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     * @return self
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlAlias()
    {
        return $this->urlAlias;
    }

    /**
     * @param string $urlAlias
     * @return self
     */
    public function setUrlAlias($urlAlias)
    {
        $this->urlAlias = $urlAlias;
        return $this;
    }

    /**
     * @return array
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @param array $startsAt
     * @return self
     */
    public function setStartsAt($startsAt)
    {
        $this->startsAt = $startsAt;
        return $this;
    }

    /**
     * @return array
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }

    /**
     * @param array $endsAt
     * @return self
     */
    public function setEndsAt($endsAt)
    {
        $this->endsAt = $endsAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @param string $rule
     * @return self
     */
    public function setRule($rule)
    {
        $this->rule = $rule;
        return $this;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     * @return self
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsAchive()
    {
        return $this->isAchive;
    }

    /**
     * @param string $isAchive
     * @return self
     */
    public function setIsAchive($isAchive)
    {
        $this->isAchive = $isAchive;
        return $this;
    }

    /**
     * @return array
     */
    public function getAdditionalFields()
    {
        return $this->additionalFields;
    }

    /**
     * @param array $additionalFields
     * @return self
     */
    public function setAdditionalFields($additionalFields)
    {
        $this->additionalFields = $additionalFields;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return array
     */
    public function getLectorIds()
    {
        return $this->lectorIds;
    }

    /**
     * @param array $lectorIds
     * @return self
     */
    public function setLectorIds($lectorIds)
    {
        $this->lectorIds = $lectorIds;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

}