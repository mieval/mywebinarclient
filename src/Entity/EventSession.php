<?php

namespace MWClient\Entity;

/**
 * Class EventSession
 * @package MWClient\Entity
 */
class EventSession extends BaseEvent
{
    /** @var string */
    private $status;

    /** @var string */
    private $eventId;

    /** @var string */
    private $estimatedAt;

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return EventSession
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param string $eventId
     * @return EventSession
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEstimatedAt()
    {
        return $this->estimatedAt;
    }

    /**
     * @param string $estimatedAt
     * @return EventSession
     */
    public function setEstimatedAt($estimatedAt)
    {
        $this->estimatedAt = $estimatedAt;
        return $this;
    }
    
    
    
}