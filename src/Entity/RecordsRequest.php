<?php

namespace MWClient\Entity;
use Exception;
use MWClient\Enum\RecordPeriod;

/**
 * Class RecordsRequest
 * @package MWClient\Entity
 */
class RecordsRequest
{
    /** @var int */
    private $id;

    /** @var string */
    private $period;

    /** @var string */
    private $from;

    /** @var string */
    private $to;

    /** @var int */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return RecordsRequest
     */
    public function setId(int $id): RecordsRequest
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param string $period
     * @return RecordsRequest
     * @throws Exception
     */
    public function setPeriod(string $period): RecordsRequest
    {
        if (!in_array($period, RecordPeriod::$enum)) {
            throw new Exception("Bad period value, given '{$period}'");
        }

        $this->period = $period;
        return $this;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     * @return RecordsRequest
     */
    public function setFrom(string $from): RecordsRequest
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     * @return RecordsRequest
     */
    public function setTo(string $to): RecordsRequest
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return RecordsRequest
     */
    public function setUserId(int $userId): RecordsRequest
    {
        $this->userId = $userId;
        return $this;
    }
}
