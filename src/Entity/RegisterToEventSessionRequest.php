<?php

namespace MWClient\Entity;

/**
 * Class RegisterToEventSessionRequest
 * @package Olymp\Bundle\WebinarBundle\Api\Entity
 */
class RegisterToEventSessionRequest extends BaseRegisterRequest
{
    /**
     * RegisterToEventSessionRequest constructor.
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }
}