<?php

namespace MWClient\Entity;

/**
 * Class EventSessions
 * @package MWClient\Entity
 */
class EventSessionInfo
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var string */
    private $status;

    /** @var int */
    private $access;

    /** @var string */
    private $lang;

    /** @var string */
    private $startsAt;

    /** @var string */
    private $utcStartsAt;

    /** @var int */
    private $createUserId;

    /** @var int */
    private $timezoneId;

    /** @var string */
    private $endsAt;

    /** @var string */
    private $duration;

    /** @var int */
    private $organizationId;

    /** @var string */
    private $type;

    /** @var string */
    private $rule;

    /** @var array */
    private $lectors = [];

    /** @var array */
    private $tags = [];

    /** @var array */
    private $announceFiles = [];

    /** @var array */
    private $files = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return EventSessionInfo
     */
    public function setId(int $id): EventSessionInfo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return EventSessionInfo
     */
    public function setName(string $name): EventSessionInfo
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return EventSessionInfo
     */
    public function setDescription(string $description): EventSessionInfo
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return EventSessionInfo
     */
    public function setStatus(string $status): EventSessionInfo
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @param int $access
     * @return EventSessionInfo
     */
    public function setAccess(int $access): EventSessionInfo
    {
        $this->access = $access;
        return $this;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return EventSessionInfo
     */
    public function setLang(string $lang): EventSessionInfo
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @param string $startsAt
     * @return EventSessionInfo
     */
    public function setStartsAt(string $startsAt): EventSessionInfo
    {
        $this->startsAt = $startsAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtcStartsAt()
    {
        return $this->utcStartsAt;
    }

    /**
     * @param string $utcStartsAt
     * @return EventSessionInfo
     */
    public function setUtcStartsAt(string $utcStartsAt): EventSessionInfo
    {
        $this->utcStartsAt = $utcStartsAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * @param int $createUserId
     * @return EventSessionInfo
     */
    public function setCreateUserId(int $createUserId): EventSessionInfo
    {
        $this->createUserId = $createUserId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTimezoneId()
    {
        return $this->timezoneId;
    }

    /**
     * @param int $timezoneId
     * @return EventSessionInfo
     */
    public function setTimezoneId(int $timezoneId): EventSessionInfo
    {
        $this->timezoneId = $timezoneId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }

    /**
     * @param string $endsAt
     * @return EventSessionInfo
     */
    public function setEndsAt(string $endsAt): EventSessionInfo
    {
        $this->endsAt = $endsAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     * @return EventSessionInfo
     */
    public function setDuration(string $duration): EventSessionInfo
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param int $organizationId
     * @return EventSessionInfo
     */
    public function setOrganizationId(int $organizationId): EventSessionInfo
    {
        $this->organizationId = $organizationId;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return EventSessionInfo
     */
    public function setType(string $type): EventSessionInfo
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @param string $rule
     * @return EventSessionInfo
     */
    public function setRule(string $rule): EventSessionInfo
    {
        $this->rule = $rule;
        return $this;
    }

    /**
     * @return array
     */
    public function getLectors()
    {
        return $this->lectors;
    }

    /**
     * @param array $lectors
     * @return EventSessionInfo
     */
    public function setLectors(array $lectors): EventSessionInfo
    {
        $this->lectors = $lectors;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return EventSessionInfo
     */
    public function setTags(array $tags): EventSessionInfo
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnnounceFiles()
    {
        return $this->announceFiles;
    }

    /**
     * @param array $announceFiles
     * @return EventSessionInfo
     */
    public function setAnnounceFiles(array $announceFiles): EventSessionInfo
    {
        $this->announceFiles = $announceFiles;
        return $this;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param array $files
     * @return EventSessionInfo
     */
    public function setFiles(array $files): EventSessionInfo
    {
        $this->files = $files;
        return $this;
    }
}
