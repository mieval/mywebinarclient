<?php

namespace MWClient\Entity;

/**
 * Class Record
 * @package MWClient\Entity
 */
class Record
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $link;

    /** @var string */
    private $isViewable;

    /** @var string */
    private $hasPassword;

    /** @var int */
    private $size;

    /** @var string */
    private $createAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Record
     */
    public function setId(int $id): Record
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Record
     */
    public function setName(string $name): Record
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Record
     */
    public function setLink(string $link): Record
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsViewable(): string
    {
        return $this->isViewable;
    }

    /**
     * @param string $isViewable
     * @return Record
     */
    public function setIsViewable(string $isViewable): Record
    {
        $this->isViewable = $isViewable;
        return $this;
    }

    /**
     * @return string
     */
    public function getHasPassword(): string
    {
        return $this->hasPassword;
    }

    /**
     * @param string $hasPassword
     * @return Record
     */
    public function setHasPassword(string $hasPassword): Record
    {
        $this->hasPassword = $hasPassword;
        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return Record
     */
    public function setSize(int $size): Record
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreateAt(): string
    {
        return $this->createAt;
    }

    /**
     * @param string $createAt
     * @return Record
     */
    public function setCreateAt(string $createAt): Record
    {
        $this->createAt = $createAt;
        return $this;
    }


}