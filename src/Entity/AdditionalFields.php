<?php

namespace MWClient\Entity;

/**
 * Class AdditionalFields
 * @package MWClient\Entity
 */
class AdditionalFields
{
    const TEXT = 'text';
    const RADIO = 'radio';

    public static $typeEnum = [
        self::TEXT,
        self::RADIO,
    ];

    /** @var string */
    private $key;

    /** @var string */
    private $label;

    /** @var string */
    private $type;

    /** @var array */
    private $values;

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return AdditionalFields
     */
    public function setKey(string $key): AdditionalFields
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return AdditionalFields
     */
    public function setLabel(string $label): AdditionalFields
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return AdditionalFields
     */
    public function setType(string $type): AdditionalFields
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param array $values
     * @return AdditionalFields
     */
    public function setValues(array $values): AdditionalFields
    {
        $this->values = $values;
        return $this;
    }
}