<?php

namespace MWClient\Entity;


/**
 * Class Event
 * @package MWClient\Entity
 */
class BaseEvent
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $isDeleted;

    /** @var string */
    protected $updateUserId;

    /** @var string */
    protected $deleteUserId;

    /** @var string */
    protected $createAt;

    /** @var string */
    protected $updateAt;

    /** @var string */
    protected $deleteAt;

    /** @var string */
    protected $lang;

    /** @var integer */
    protected $access;

    /** @var string */
    protected $password;

    /** @var string */
    protected $createUserId;

    /** @var mixed */
    protected $createUser;

    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    /** @var string */
    protected $timezoneId;

    /** @var string */
    protected $timezone;

    /** @var string */
    protected $startsAt;

    /** @var string */
    protected $utcStartsAt;

    /** @var string */
    protected $endsAt;

    /** @var string */
    protected $duration;

    /** @var array */
    protected $additionalFields;

    /** @var string */
    protected $isArchived;

    /** @var string */
    protected $imageId;

    /** @var string */
    protected $image;

    /** @var string */
    protected $organizationId;

    /** @var mixed */
    protected $organization;

    /** @var array */
    protected $lectors;

    /** @var array */
    protected $announceFiles;

    /** @var array */
    protected $tags;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param string $isDeleted
     * @return self
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdateUserId()
    {
        return $this->updateUserId;
    }

    /**
     * @param string $updateUserId
     * @return self
     */
    public function setUpdateUserId($updateUserId)
    {
        $this->updateUserId = $updateUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeleteUserId()
    {
        return $this->deleteUserId;
    }

    /**
     * @param string $deleteUserId
     * @return self
     */
    public function setDeleteUserId($deleteUserId)
    {
        $this->deleteUserId = $deleteUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * @param string $createAt
     * @return self
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * @param string $updateAt
     * @return self
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeleteAt()
    {
        return $this->deleteAt;
    }

    /**
     * @param string $deleteAt
     * @return self
     */
    public function setDeleteAt($deleteAt)
    {
        $this->deleteAt = $deleteAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return self
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @param int $access
     * @return self
     */
    public function setAccess($access)
    {
        $this->access = $access;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * @param string $createUserId
     * @return self
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateUser()
    {
        return $this->createUser;
    }

    /**
     * @param mixed $createUser
     * @return self
     */
    public function setCreateUser($createUser)
    {
        $this->createUser = $createUser;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimezoneId()
    {
        return $this->timezoneId;
    }

    /**
     * @param string $timezoneId
     * @return self
     */
    public function setTimezoneId($timezoneId)
    {
        $this->timezoneId = $timezoneId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     * @return self
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @param string $startsAt
     * @return self
     */
    public function setStartsAt($startsAt)
    {
        $this->startsAt = $startsAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtcStartsAt()
    {
        return $this->utcStartsAt;
    }

    /**
     * @param string $utcStartsAt
     * @return self
     */
    public function setUtcStartsAt($utcStartsAt)
    {
        $this->utcStartsAt = $utcStartsAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }

    /**
     * @param string $endsAt
     * @return self
     */
    public function setEndsAt($endsAt)
    {
        $this->endsAt = $endsAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     * @return self
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return array
     */
    public function getAdditionalFields()
    {
        return $this->additionalFields;
    }

    /**
     * @param array $additionalFields
     * @return self
     */
    public function setAdditionalFields($additionalFields)
    {
        $this->additionalFields = $additionalFields;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsArchived()
    {
        return $this->isArchived;
    }

    /**
     * @param string $isArchived
     * @return self
     */
    public function setIsArchived($isArchived)
    {
        $this->isArchived = $isArchived;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param string $imageId
     * @return self
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param string $organizationId
     * @return self
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     * @return self
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return array
     */
    public function getLectors()
    {
        return $this->lectors;
    }

    /**
     * @param array $lectors
     * @return self
     */
    public function setLectors($lectors)
    {
        $this->lectors = $lectors;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnnounceFiles()
    {
        return $this->announceFiles;
    }

    /**
     * @param array $announceFiles
     * @return self
     */
    public function setAnnounceFiles($announceFiles)
    {
        $this->announceFiles = $announceFiles;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }
}
