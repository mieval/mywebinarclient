<?php

namespace MWClient\Entity;

/**
 * Class BaseRegisterRequest
 * @package Olymp\Bundle\WebinarBundle\Api\Entity
 */
class BaseRegisterRequest
{
    /** @var string $email */
    protected $email;

    /** @var string $role */
    protected $role;

    /** @var string $isAutoEnter */
    protected $isAutoEnter;

    /** @var string $sendEmail */
    protected $sendEmail;

    /** @var string $date */
    protected $date; // only 'yyyy-mm-dd' format

    /** @var string $nickname */
    protected $nickname;

    /** @var string $name */
    protected $name;

    /** @var string $secondName */
    protected $secondName;

    /** @var string $pattrName */
    protected $pattrName;

    /** @var string $phone */
    protected $phone;

    /** @var string $description */
    protected $description;

    /** @var string $organization */
    protected $organization;

    /** @var string $position */
    protected $position;

    /** @var string $sex */
    protected $sex;

    /** @var string $avatar */
    protected $avatar; // image url

    /** @var array $additionalFields */
    protected $additionalFields;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return self
     */
    public function setRole(string $role): self
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsAutoEnter()
    {
        return $this->isAutoEnter;
    }

    /**
     * @param string $isAutoEnter
     * @return self
     */
    public function setIsAutoEnter(string $isAutoEnter): self
    {
        $this->isAutoEnter = $isAutoEnter;
        return $this;
    }

    /**
     * @return string
     */
    public function getSendEmail()
    {
        return $this->sendEmail;
    }

    /**
     * @param string $sendEmail
     * @return self
     */
    public function setSendEmail(string $sendEmail): self
    {
        $this->sendEmail = $sendEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return self
     */
    public function setDate(string $date): self
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return self
     */
    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * @param string $secondName
     * @return self
     */
    public function setSecondName(string $secondName): self
    {
        $this->secondName = $secondName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPattrName()
    {
        return $this->pattrName;
    }

    /**
     * @param string $pattrName
     * @return self
     */
    public function setPattrName(string $pattrName): self
    {
        $this->pattrName = $pattrName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return self
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param string $organization
     * @return self
     */
    public function setOrganization(string $organization): self
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     * @return self
     */
    public function setPosition(string $position): self
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     * @return self
     */
    public function setSex(string $sex): self
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return self
     */
    public function setAvatar(string $avatar): self
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return array
     */
    public function getAdditionalFields()
    {
        return $this->additionalFields;
    }

    /**
     * @param array $additionalFields
     * @return self
     */
    public function setAdditionalFields(array $additionalFields): self
    {
        $this->additionalFields = $additionalFields;
        return $this;
    }
}