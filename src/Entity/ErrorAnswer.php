<?php

namespace MWClient\Entity;

/**
 * Class ErrorAnswer
 * @package MWClient\Entity
 */
class ErrorAnswer
{
    /** @var int */
    private $code;

    /** @var string */
    private $message;

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return ErrorAnswer
     */
    public function setCode(int $code): ErrorAnswer
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ErrorAnswer
     */
    public function setMessage(string $message): ErrorAnswer
    {
        $this->message = $message;
        return $this;
    }
}
