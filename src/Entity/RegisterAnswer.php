<?php

namespace MWClient\Entity;

/**
 * Class RegisterAnswer
 * @package Entity
 */
class RegisterAnswer
{
    /** @var string $participationId */
    private $participationId;

    /** @var string $link */
    private $link;

    /** @var string $contactId */
    private $contactId;

    /**
     * @return string
     */
    public function getParticipationId(): string
    {
        return $this->participationId;
    }

    /**
     * @param string $participationId
     * @return RegisterAnswer
     */
    public function setParticipationId(string $participationId): RegisterAnswer
    {
        $this->participationId = $participationId;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return RegisterAnswer
     */
    public function setLink(string $link): RegisterAnswer
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactId(): string
    {
        return $this->contactId;
    }

    /**
     * @param string $contactId
     * @return RegisterAnswer
     */
    public function setContactId(string $contactId): RegisterAnswer
    {
        $this->contactId = $contactId;
        return $this;
    }
}