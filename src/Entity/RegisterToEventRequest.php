<?php

namespace MWClient\Entity;

/**
 * Class RegisterToEventRequest
 * @package Olymp\Bundle\WebinarBundle\Api\Entity
 */
class RegisterToEventRequest extends BaseRegisterRequest
{
    /**
     * RegisterToEventRequest constructor.
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }
}