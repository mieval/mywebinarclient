<?php

namespace MWClient\Entity;

use Exception;
use MWClient\Enum\Access;

/**
 * Class CreateEventRequest
 * @package MWClient\Entity
 */
class CreateEventRequest extends EventRequest
{
    /**
     * CreateEventRequest constructor.
     * @param string $eventName
     * @param int $access
     * @throws Exception
     */
    public function __construct(string $eventName, int $access)
    {
        if (empty(trim($eventName))) {
            throw new Exception("Event name is empty");
        }

        if (!in_array($access, Access::$enum)) {
            throw new Exception("Bad access value, given '{$access}'");
        }

        $this->name = $eventName;
        $this->access = $access;
    }
}