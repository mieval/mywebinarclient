<?php

namespace MWClient;

use Buzz\Browser;
use Buzz\Message\Response;
use Exception;
use MWClient\Entity\CreateEventAnswer;
use MWClient\Entity\ErrorAnswer;
use MWClient\Entity\EventInfo;
use MWClient\Entity\EventRequest;
use MWClient\Entity\EventSessionInfo;
use MWClient\Entity\Record;
use MWClient\Entity\RecordsRequest;
use MWClient\Entity\RegisterAnswer;
use MWClient\Entity\RegisterToEventRequest;
use MWClient\Entity\RegisterToEventSessionRequest;
use MWClient\Entity\UpdateEventRequest;
use MWClient\Enum\Files;
use MWClient\Helpers\Utils;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MyWebinarClient
 * @package src
 */
class MyWebinarClient
{
    /** @var string */
    private $key;

    /** @var string */
    private $serviceUrl;

    /** @var Browser */
    private $browser;

    const loadEventInfo = '/organization/events/%d';
    const loadEventSessionInfo = '/eventsessions/%d';
    const registerToEvent = '/events/%s/register';
    const registerToEventSession = '/eventsessions/%s/register';
    const createEvent = '/events';
    const updateEvent = '/organization/events/%s';
    const deleteEvent = '/organization/events/%s';
    const createEventSession = '/events/%s/sessions';
    const updateEventSession = '/eventsessions/%s';
    const loadRecordsList = '/records';


    /**
     * @param Browser $browser
     * @return self
     */
    public function setBrowser(Browser $browser): self
    {
        $this->browser = $browser;
        return $this;
    }


    /**
     * MyWebinarApi constructor.
     *
     * @param string $key - API key
     * @param string $url - base url for API request
     * @throws Exception
     */
    public function __construct(string $key, string $url)
    {
        $key = trim($key);
        $url = trim($url);

        $errors = Utils::isValid($key, [
            new Assert\NotBlank(),
        ]);

        if (count($errors)) {
            throw new Exception("Bad service url value");
        }

        $errors = Utils::isValid($url, [
            new Assert\NotBlank(),
            new Assert\Url(),
        ]);

        if (count($errors)) {
            throw new Exception("Bad service url value");
        }

        $this->key = $key;
        $this->serviceUrl = $url;
    }


    /**
     * Return base headers for request
     *
     * @return array
     */
    private function getBaseHeaders()
    {
        return [
            'X-Auth-Token' => $this->key,
        ];
    }


    /**
     * Create new event
     *
     * @param EventRequest $request
     * @return CreateEventAnswer
     * @throws Exception
     */
    public function createEvent(EventRequest $request)
    {
        $payload = Utils::getEntityProperties($request, true);
        $payload = http_build_query($payload);

        /** @var Response $response */
        $response = $this->browser->post(
            $this->serviceUrl . self::createEvent,
            $this->getBaseHeaders(),
            $payload
        );

        if (!$response->isSuccessful()) {
            $error = $this->getErrorAnswer($response->getContent());
            throw new Exception("Error while event create: " . $error->getMessage());
        }

        return Utils::fillObject(new CreateEventAnswer, json_decode($response->getContent(), true));
    }


    /**
     * Create event
     *
     * @param $eventId
     * @param UpdateEventRequest $request
     * @throws Exception
     */
    public function updateEvent($eventId, UpdateEventRequest $request)
    {
        $payload = Utils::getEntityProperties($request, true);
        $payload = http_build_query($payload);

        /** @var Response $response */
        $response = $this->browser->put(
            $this->serviceUrl . sprintf(self::updateEvent, $eventId),
            $this->getBaseHeaders(),
            $payload
        );

        if (!$response->isSuccessful()) {
            /** @var ErrorAnswer $error */
            $error = $this->getErrorAnswer($response->getContent());
            throw new Exception("Error while event update: " . $error->getMessage());
        }
    }


    /**
     * Delete event
     *
     * @param int $eventId
     * @throws Exception
     */
    public function deleteEvent(int $eventId)
    {
        $this->checkEventId($eventId);

        /** @var Response $response */
        $response = $this->browser->delete(
            $this->serviceUrl . sprintf(self::deleteEvent, $eventId),
            $this->getBaseHeaders()
        );

        if (!$response->isSuccessful()) {
            /** @var ErrorAnswer $error */
            $error = $this->getErrorAnswer($response->getContent());
            throw new Exception("Error while event delete: " . $error->getMessage());
        }
    }


    /**
     * Loading event information by given id
     *
     * @param int $eventId - event id
     * @return EventInfo
     * @throws Exception
     */
    public function loadEventInfo(int $eventId)
    {
        $this->checkEventId($eventId);

        /** @var Response $response */
        $response = $this->browser->get(
            $this->serviceUrl . sprintf(self::loadEventInfo, $eventId),
            $this->getBaseHeaders()
        );

        $responseMessage = $response->getContent();

        if (!$response->isSuccessful()) {
            $error = $this->getErrorAnswer($responseMessage);
            throw new Exception("Loading event '{$eventId}': " . $error->getMessage());
        }

        /** @var EventInfo $eventInfo */
        $eventInfo = Utils::fillObject(new EventInfo, json_decode($responseMessage, true));

        $sessionsInfo = array_map(function($rawEventSessionInfo) {
            return Utils::fillObject(new EventSessionInfo, $rawEventSessionInfo);
        }, $eventInfo->getEventSessions());

        $eventInfo->setEventSessions($sessionsInfo);

        return $eventInfo;
    }


    /**
     * Loading record url for event
     *
     * @param int $eventId
     * @return string
     * @throws Exception
     */
    public function getWebinarRecordUrl(int $eventId): string
    {
        /** @var EventInfo $eventInfo */
        $eventInfo = $this->loadEventInfo($eventId);

        /** @var EventSessionInfo[] $eventSessionsInfoList */
        $eventSessionsInfoList = $eventInfo->getEventSessions();

        if (empty($eventSessionsInfoList)) {
            throw new Exception("Event session list is empty");
        }

        /** @var EventSessionInfo $eventSessionsInfo */
        $eventSessionsInfo = current($eventSessionsInfoList);

        /** @var array $files */
        $files = $eventSessionsInfo->getFiles();

        $fileId = null;

        foreach ($files as $file) {
            if (Files::RECORD === $file['fileType']) {
                $fileId = $file['id'];
                break;
            }
        }

        if (null === $fileId) {
            throw new Exception("Record for event not found");
        }

        $recordRequest = (new RecordsRequest)->setId($fileId);

        /** @var Record[] $record */
        $records = $this->loadRecords($recordRequest);

        if (empty($records)) {
            throw new Exception();
        }

        /** @var Record $record */
        $record = current($records);

        return $record->getLink();
    }


    /**
     * Loading event information by given id
     *
     * @param string $eventSessionId - event id
     * @return string
     * @throws Exception
     */
    public function loadEventSessionInfo(string $eventSessionId)
    {
        $this->checkEventId($eventSessionId);

        /** @var Response $response */
        $response = $this->browser->get(
            $this->serviceUrl . sprintf(self::loadEventSessionInfo, $eventSessionId),
            $this->getBaseHeaders()
        );

        if (!$response->isSuccessful()) {
            $error = $this->getErrorAnswer($response->getContent());
            throw new Exception("Loading event session '{$eventSessionId}': " . $error->getMessage());
        }

        return json_decode($response->getContent());
    }


    /**
     * Register user to event
     *
     * @param string $eventId
     * @param RegisterToEventRequest $registerRequest
     * @return RegisterAnswer
     * @throws Exception
     */
    public function registrationToEvent(string $eventId, RegisterToEventRequest $registerRequest)
    {
        $this->checkEventId($eventId);
        $payload = Utils::getEntityProperties($registerRequest, true);
        $payload = http_build_query($payload);

        /** @var Response $response */
        $response = $this->browser->post(
            $this->serviceUrl . sprintf(self::registerToEvent, $eventId),
            $this->getBaseHeaders(),
            $payload
        );

        if (!$response->isSuccessful()) {
            $error = $this->getErrorAnswer($response->getContent());
            throw new Exception("Error while register to event '{$eventId}': " . $error->getMessage());
        }

        return Utils::fillObject(new RegisterAnswer, json_decode($response->getContent(), true));
    }


    /**
     * Register user to event session
     *
     * @param string $eventSessionId
     * @param RegisterToEventSessionRequest $registerRequest
     * @return RegisterAnswer
     * @throws Exception
     */
    public function registrationToEventSession(string $eventSessionId, RegisterToEventSessionRequest $registerRequest)
    {
        $this->checkEventId($eventSessionId);
        $payload = Utils::getEntityProperties($registerRequest, true);
        $payload = http_build_query($payload);

        /** @var Response $response */
        $response = $this->browser->post(
            $this->serviceUrl . sprintf(self::registerToEventSession, $eventSessionId),
            $this->getBaseHeaders(),
            $payload
        );

        if (!$response->isSuccessful()) {
            $error = $this->getErrorAnswer($response->getContent());
            throw new Exception("Error while register to event session'{$eventSessionId}': " . $error->getMessage());
        }

        return Utils::fillObject(new RegisterAnswer, json_decode($response->getContent(), true));
    }


    /**
     * Loading record list
     *
     * @param RecordsRequest|null $recordRequest
     * @return Record[]
     * @throws Exception
     */
    public function loadRecords(RecordsRequest $recordRequest = null)
    {
        $payload = [];

        if (null !== $recordRequest) {
            $payload = Utils::getEntityProperties($recordRequest, true);
        }

        $payload = empty($payload) ? '' : "?" . http_build_query($payload);

        /** @var Response $response */
        $response = $this->browser->get(
            $this->serviceUrl . self::loadRecordsList . "?" . $payload,
            $this->getBaseHeaders()
        );

        $responseMessage = $response->getContent();

        if (!$response->isSuccessful()) {
            $error = $this->getErrorAnswer($responseMessage);
            throw new Exception("Error while records loading: " . $error->getMessage());
        }

        $rawRecordsList = json_decode($responseMessage, true);

        return array_map(function ($rawRecord) {
            return Utils::fillObject(new Record, $rawRecord);
        }, $rawRecordsList);

    }


    /**
     * @param string $eventSessionId
     * @throws Exception
     */
    private function checkEventId(string $eventSessionId)
    {
        if (empty(trim($eventSessionId))) {
            throw new Exception("ID value is empty");
        }
    }


    /**
     * Decode error message
     *
     * @param string $responseMessage
     * @return ErrorAnswer
     */
    private function getErrorAnswer(string $responseMessage)
    {
        $data = json_decode($responseMessage, true);
        return Utils::fillObject(new ErrorAnswer, $data['error']);
    }
}
