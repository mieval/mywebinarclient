<?php

namespace MWClient\Helpers;

use DateTime;
use Exception;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Utils
{

    /** @var null|ValidatorInterface */
    private static $validator = null;


    /**
     * Validator singleton
     *
     * @return ValidatorInterface
     */
    public static function getValidator()
    {
        if (null === self::$validator) {
            self::$validator = Validation::createValidatorBuilder()->getValidator();
        }

        return self::$validator;
    }


    /**
     * @param $value
     * @param array $constraints
     * @return ConstraintViolationListInterface
     */
    public static function isValid($value, array $constraints)
    {
        $validator = self::getValidator();
        return $validator->validate($value, $constraints);
    }


    /**
     * Fill class instance
     *
     * @param $instance
     * @param array $data
     * @return mixed
     */
    public static function fillObject($instance, array $data)
    {
        foreach ($data as $field => $value) {
            $fieldName = str_replace(' ', '', ucwords(str_replace('_', ' ', $field)));
            $setMethodName = 'set'.ucfirst($fieldName);

            if (isset($value) && method_exists($instance, $setMethodName)) {
                $instance->$setMethodName($value);
            }
        }

        return $instance;
    }


    /**
     * @param $instance - entity class instance
     * @param bool $filledOnly - delete null,
     * @return array
     */
    public static function getEntityProperties($instance, $filledOnly = false): array
    {
        $className = get_class($instance);
        $class = new ReflectionClass($className);

        /** @var ReflectionProperty[] $properties */
        $properties = $class->getProperties();

        $propertiesList = [];

        foreach($properties as $property) {

            /** @var ReflectionProperty $property */
            $propertyName = $property->getName();

            $getter = 'get' . ucfirst($propertyName);

            if (method_exists($instance, $getter)) {
                $value = $instance->$getter();
                $propertiesList[$propertyName] = $value;
            }

        }

        if ($filledOnly) {
            $propertiesList = array_filter($propertiesList);
        }

        return $propertiesList;
    }


    /**
     * Convert datetime string to webinar format
     *
     * @param string $dateTime
     * @return array
     * @throws Exception
     */
    public static function getDateInWebinarFormat(string $dateTime)
    {
        if (empty(trim($dateTime))) {
            throw new Exception("Datetime string is empty");
        }

        $dateTime = new DateTime($dateTime);

        return [
            'date' => [
                'year' => (int)$dateTime->format('Y'),
                'month' => (int)$dateTime->format('m'),
                'day' => (int)$dateTime->format('d')
            ],
            'time' => [
                'hour' => (int)$dateTime->format('H'),
                'minute' => (int)$dateTime->format('i')
            ]
        ];
    }
}